# BA_SoftwareProject

Small Software Project in the 5th semester of Bachelor of Applied Science in BA Leipzig.

This project contains an android application for saving, editing and filtering recipes.

General project structure:
The main directory contains the res and the java folder and a manifest file.
- manifest file: Informs the application about general preferences and the relation of the activities
- content res: All static files  which are frequently referenced like layout files, images and icons, text, style and other values (mainly in XML format)
- content java: Contains all code files
    - data directory: Any code, related to the model, database, api-call
    - helper directory: Util classes and files which are needed all over the place
    - view directory: All code needed for frontend rendering (views, viewmodel, recyclerviews)


Source of placeholder picture:
https://p0.pxfuel.com/preview/229/404/1003/food-and-drink-background-backgrounds-pasta.jpg
from
https://www.pxfuel.com/en/free-photo-jmrml