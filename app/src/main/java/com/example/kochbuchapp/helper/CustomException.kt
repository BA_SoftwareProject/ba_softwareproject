package com.example.kochbuchapp.helper

import java.lang.Exception

class ApiException (message : String) : Exception(message)