package com.example.kochbuchapp.helper

import android.content.res.Resources

// helper function to convert pixel-values to dp-values and vice versa
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()