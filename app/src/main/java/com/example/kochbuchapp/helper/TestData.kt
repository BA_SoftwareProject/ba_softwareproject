package com.example.kochbuchapp.helper

import android.content.Context
import com.example.kochbuchapp.data.api.RestAPIClient
import com.example.kochbuchapp.data.model.IngredientModel
import com.example.kochbuchapp.data.model.RecipeImageModel
import com.example.kochbuchapp.data.model.RecipeModel
import com.example.kochbuchapp.data.model.TagModel
import com.example.kochbuchapp.data.repository.RecipeRepository
import kotlinx.coroutines.runBlocking
import java.time.LocalDateTime

class TestData(context : Context) {
    private val apiClient = RestAPIClient()
    private val repository = RecipeRepository(context.applicationContext)

    fun getRecipe() : RecipeModel {
        return RecipeModel(
            id = 1L,
            title = "Test-Rezept",
            subtitle = "Das ist nur ein Testrezept",
            image = RecipeImageModel(
                imageId = 42,
                hasImage = true,
                isLocal = true,
                reference = "src/main/res/drawable/placeholder600x400.png"
            ),
            createdAt = LocalDateTime.now().toString(),
            preparationTime = 15,
            cookingTime = 25,
            portions = 4,
            ingredients = mutableListOf(
                IngredientModel(
                    ingredientId = 666L,
                    recipeId = 1L,
                    name = "Mehl",
                    unit = "g",
                    amount = 200.0,
                    productGroup = "Mehl, Zucker und Co"
                ),
                IngredientModel(
                    ingredientId = 999L,
                    recipeId = 1L,
                    name = "Öl",
                    unit = "Tasse",
                    amount = 0.5,
                    productGroup = "Backmaterialien"
                ),
                IngredientModel(
                    ingredientId = 696L,
                    recipeId = 1L,
                    name = "Fischbeine",
                        unit = "Portion",
                        amount = 1.0,
                        productGroup = "Reste"
                ),
                IngredientModel(
                    ingredientId = 969L,
                    recipeId = 1L,
                    name = "Ahornsirup",
                    unit = "Liter",
                    amount = 1.5,
                    productGroup = null
                )
            ),
            instructions = mutableListOf(
                "Zuerst werden alle Zutaten zusammengerührt",
                "Dann in die Pfanne damit",
                "Fertig ist das leckere, warme Ölmehl!",
                "Nicht vergessen den Ahornsirup darüberzugeben. Sonst werden die Beeren nicht so taufrisch. (Tipp von Johann!!)"
            ),
            tags = mutableListOf(
                TagModel(
                    tagId = 123,
                    name = "ungenießbar",
                    listPosition = null,
                    isChecked = false
                ),
                TagModel(
                    tagId = 124,
                    name = "eklig",
                    listPosition = null,
                    isChecked = false
                ),
                TagModel(
                    tagId = 125,
                    name = "nicht schwierig",
                    listPosition = null,
                    isChecked = false
                )
            ),
            comments = mutableListOf(
                "Muss man unbedingt probiert haben!",
                "Hier ist noch ein zweites Kommentar"
            ),
            siteUrl = null
        )
    }

    fun fillDataBase() {
        runBlocking {
            repository.saveRecipe(apiClient.findRecipe("1597871267020573", null)!!) // Hackbällchen Toscana
            repository.saveRecipe(apiClient.findRecipe("1613999850871", null)!!)    // Erbseneintopf
            repository.saveRecipe(apiClient.findRecipe("1844061298739441", null)!!) // Mozarella-Hähnchen
            repository.saveRecipe(apiClient.findRecipe("3176741472801424", null)!!) // Wackelpudding-Melone
            repository.saveRecipe(apiClient.findRecipe("108971045666750", null)!!)  // Himbeer-Baiser-Sahne Desert
            repository.saveRecipe(apiClient.findRecipe("40451013607727", null)!!)   // ungarischer Kesselgulasch
            repository.saveRecipe(apiClient.findRecipe("549221152542256", null)!!)  // Backfisch
            repository.saveRecipe(apiClient.findRecipe("1901451309872830", null)!!) // orientalischer Couscous-Salat
            repository.saveRecipe(apiClient.findRecipe("1263011231585840", null)!!) // Schwäbische Flädlesupp
            repository.saveRecipe(apiClient.findRecipe("3868221588750390", null)!!) // Bourbon Burger
            repository.saveRecipe(apiClient.findRecipe("1394891244393013", null)!!) // Gegrillte Teigspieße
            repository.saveRecipe(apiClient.findRecipe("1953131317830499", null)!!) // Kürbis Gnocchi Auflauf
            repository.saveRecipe(apiClient.findRecipe("3031051455827110", null)!!) // Griechischer Salat
            repository.saveRecipe(apiClient.findRecipe("1996341323284940", null)!!) // Reis-Schichtsalat

        }
    }
}

