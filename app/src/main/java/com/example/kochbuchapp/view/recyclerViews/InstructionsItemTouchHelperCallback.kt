package com.example.kochbuchapp.view.recyclerViews

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class InstructionsItemTouchHelperCallback(instructionsRecycleViewAdapter: InstructionsRecycleViewAdapter): ItemTouchHelper.Callback() {

    private val mInstructionsRecycleViewAdapter = instructionsRecycleViewAdapter

    // gets the movement flags for drag'n'drop: up and down movement
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG, ItemTouchHelper.UP or ItemTouchHelper.DOWN)
    }

    // called whenever an item is swapped with another
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        mInstructionsRecycleViewAdapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    // disables drag&drop for long pressing
    override fun isLongPressDragEnabled(): Boolean {
        return false
    }

    // called whenever an item is swiped to the side. Not used here, but needs to be implemented
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    }

    // called whenever an item is starting to get dragged -> changes color to highlight dragged item and updates the tagList
    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            val itemViewHolder = viewHolder as InstructionsRecycleViewAdapter.InstructionsViewHolder
            itemViewHolder.onItemSelected()
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    // called whenever an item is "dropped" -> changes color back to normal
    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        val itemViewHolder = viewHolder as InstructionsRecycleViewAdapter.InstructionsViewHolder
        itemViewHolder.onItemClear()
    }
}