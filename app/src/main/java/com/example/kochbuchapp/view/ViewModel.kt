package com.example.kochbuchapp.view

import android.app.Application
import androidx.lifecycle.*
import com.example.kochbuchapp.data.model.RecipeModel
import com.example.kochbuchapp.data.model.RecipeWithIngredientsAndTags
import com.example.kochbuchapp.data.model.TagModel
import com.example.kochbuchapp.data.repository.RecipeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class ViewModel(application: Application): AndroidViewModel(application) {

    val recipeLiveData: LiveData<List<RecipeWithIngredientsAndTags>>
    val tagLiveData: LiveData<List<TagModel>>
    private val repository: RecipeRepository = RecipeRepository(application)

    init {
        recipeLiveData = repository.getRecipesLd()
        tagLiveData = repository.getTags()
    }

    fun saveRecipe(recipe: RecipeModel,
                   updateTags : Boolean = true,
                   updateIngredients : Boolean = true
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveRecipe(recipe, updateTags, updateIngredients)
        }
    }

    fun deleteRecipe(recipe : RecipeModel) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteRecipe(recipe)
        }
        recipe.image?.let {
            if (recipe.image!!.hasImage) {
                if (File(recipe.image!!.reference).exists()) {
                    File(recipe.image!!.reference).delete()
                }
            }
        }
    }

    fun deleteTag(tagName : String, recipeId : Long) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteTag(tagName, recipeId)
        }
    }
}