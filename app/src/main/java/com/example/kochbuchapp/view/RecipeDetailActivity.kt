package com.example.kochbuchapp.view

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isNotEmpty
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.bumptech.glide.Glide
import com.example.kochbuchapp.R
import com.example.kochbuchapp.helper.px
import com.example.kochbuchapp.data.model.RecipeModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_recipe_create.*
import kotlinx.android.synthetic.main.activity_recipe_detail.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.math.BigDecimal
import java.math.RoundingMode

@SuppressLint("SetTextI18N")
class RecipeDetailActivity : AppCompatActivity(), ViewModelStoreOwner, PopupMenu.OnMenuItemClickListener {
    private lateinit var viewModel: ViewModel
    private lateinit var recipe : RecipeModel

    override fun onCreate(savedInstanceState: Bundle?) {
        // Get the recipe given as parameter (see clickListener in RecipeListActivity)
        recipe = Gson().fromJson(
            intent.getStringExtra("recipe"),
            RecipeModel::class.java
        )
        createViewModel()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_detail)

        // Set content dynamically from recipe parameter
        txt_recipeTitle.text   = recipe.title
        txt_prepTime.text      = "${recipe.preparationTime} min"
        txt_cookTime.text      = "${recipe.cookingTime} min"
        txt_servingsCount.text = "${recipe.portions}"
        recipe.image?.let {
            if(it.hasImage) {
                Glide.with(this).load(it.reference).into(img_recipe)
            }
        }
        // Build Lists
        showIngredients(recipe)
        showInstructions(recipe)

        if (recipe.comments.isNullOrEmpty() || recipe.comments[0] == "")
            section_comment.visibility = ConstraintLayout.GONE
        else showComments(recipe)

        if (recipe.tags.isNullOrEmpty() || recipe.tags[0].name == "")
            section_tag.visibility = ConstraintLayout.GONE
        else showTags(recipe)

        // remove preparation-divider if both subsequent sections are missing
        if ((recipe.comments.isNullOrEmpty() || recipe.comments[0] == "") &&
            (recipe.tags.isNullOrEmpty() || recipe.tags[0].name == "")) {
            div_preparation.visibility = ConstraintLayout.GONE
        }

        // Add Button Click-Listeners
        btn_back.setOnClickListener { onSupportNavigateUp() }
        btn_edit.setOnClickListener { showPopup(btn_edit) }

        imageButtonArrowDown.setOnClickListener {
            if (recipe.portions != null && recipe.portions!! > 1) {
                val newPortionsAmount : Double = recipe.portions!!.toDouble()-1.0
                recipe.ingredients.forEach { ingredient ->
                            if (ingredient.amount != 0.0) {
                                ingredient.amount *= (newPortionsAmount / recipe.portions!!.toDouble())
                                ingredient.amount = BigDecimal(ingredient.amount).setScale(2, RoundingMode.HALF_EVEN).toDouble()
                            }
                    }
                recipe.portions = recipe.portions!! -1
                txt_servingsCount.text = "${recipe.portions}"
                showIngredients(recipe)
                }
        }

        imageButtonArrowUp.setOnClickListener {
            if (recipe.portions != null) {
                val newPortionsAmount : Double = recipe.portions!!.toDouble()+1.0
                recipe.ingredients.forEach { ingredient ->
                    if (ingredient.amount != 0.0) {
                        ingredient.amount *= (newPortionsAmount / recipe.portions!!.toDouble())
                        ingredient.amount = BigDecimal(ingredient.amount).setScale(2, RoundingMode.HALF_EVEN).toDouble()
                    }
                }
                recipe.portions = recipe.portions!! +1
                txt_servingsCount.text = "${recipe.portions}"
                showIngredients(recipe)
            }
        }
    }

    private fun showIngredients(recipe: RecipeModel) {
        if(list_ingredients.isNotEmpty()) { list_ingredients.removeAllViewsInLayout() }

        recipe.ingredients.forEach { ingredient ->
            // Für jede Zutat ein RelativeLayout erstellen (listItem) zum organisieren der TextViews
            val listItem = RelativeLayout(list_ingredients.context).apply {
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
            }

            // TextView für Menge und Einheit der Zutat
            val ingredientAmountUnit = TextView(listItem.context).apply {
                text = "${if (ingredient.amount % 1.0 == 0.0) {
                            if (ingredient.amount != 0.0)
                            ingredient.amount.toInt() else "" } 
                        else ingredient.amount} ${ingredient.unit}"
                layoutParams = RelativeLayout.LayoutParams(
                    160.px,
                    RelativeLayout.LayoutParams.MATCH_PARENT
                ).apply {
                    addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                }
                id = View.generateViewId()
                setPadding(0, 4.px, 0, 0)
                textSize = 24F
                setTextColor(ResourcesCompat.getColor(resources, R.color.colorTextDarkGray, null))
                setTypeface(resources.getFont(R.font.montserrat), Typeface.BOLD)
                textAlignment =  View.TEXT_ALIGNMENT_TEXT_END
            }

            // TextView für Zutatenname
            val ingredientName = TextView(listItem.context).apply {
                text = ingredient.name
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT
                ).apply {
                    addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    addRule(RelativeLayout.RIGHT_OF, ingredientAmountUnit.id)
                }
                id = View.generateViewId()
                setPadding(18.px, 4.px, 0, 0)
                textSize = 24F
                setTextColor(ResourcesCompat.getColor(resources, R.color.colorTextDefault, null))
                typeface = resources.getFont(R.font.montserrat)
                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
            }

            list_ingredients.addView(listItem)
            listItem.addView(ingredientAmountUnit)
            listItem.addView(ingredientName)
        }
    }

    private fun showInstructions(recipe: RecipeModel) {
        recipe.instructions.forEachIndexed { index, instruction ->
            val listItem = RelativeLayout(list_instructions.context).apply {
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
            }

            val instructionNumber = TextView(listItem.context).apply {
                background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.bg_list_numerator,
                    null
                )
                layoutParams = RelativeLayout.LayoutParams(
                    24.px,
                    24.px
                ).apply {
                    addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                    setMargins(18.px, 5.px, 0, 16)
                }
                text = "${index + 1}"
                setTextColor(ContextCompat.getColor(this.context, R.color.colorDarkWhite))
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_GRAVITY
                includeFontPadding = false
                id = View.generateViewId()
                textSize = 14F
                setTypeface(resources.getFont(R.font.montserrat), Typeface.BOLD)
            }

            val instructionText = TextView(listItem.context).apply {
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT
                ).apply {
                    addRule(RelativeLayout.ALIGN_PARENT_TOP)
                    addRule(RelativeLayout.RIGHT_OF, instructionNumber.id)
                    setMargins(50, 0, 70, 16)
                }
                setTextColor(ResourcesCompat.getColor(resources, R.color.colorTextDefault, null))
                text = instruction
                id = View.generateViewId()
                typeface = resources.getFont(R.font.montserrat)
                textSize = 20F
            }
            list_instructions.addView(listItem)
            listItem.addView(instructionText)
            listItem.addView(instructionNumber)
        }
    }

    private fun showComments(recipe: RecipeModel) {
        recipe.comments.forEach { comment ->
            val commentText = TextView(list_comments.context).apply {
                text = comment
                id = View.generateViewId()
                typeface = resources.getFont(R.font.montserrat)
                textSize = 20F
                setTextColor(ResourcesCompat.getColor(resources, R.color.colorTextDefault, null))
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                ).apply {setMargins(30.px, 0, 30.px, 16)}
            }
            list_comments.addView(commentText)
        }
    }

    private fun showTags(recipe: RecipeModel) {
        recipe.tags.forEach { tag ->
            val tagItem = RelativeLayout(list_instructions.context).apply {
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    setMargins(30.px, 0, 0, 16)
                }
                background = ResourcesCompat.getDrawable(resources, R.drawable.bg_tag_box, null)
            }

            val tagText = TextView(list_tags.context).apply {
                text = tag.name
                typeface = resources.getFont(R.font.montserrat)
                textSize = 20F
                gravity = Gravity.CENTER
                textAlignment = View.TEXT_ALIGNMENT_GRAVITY
                setTextColor(ResourcesCompat.getColor(resources, R.color.colorTextDefault, null))
                id = View.generateViewId()
                layoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    35.px
                ).apply {
                    addRule(RelativeLayout.CENTER_VERTICAL)
                    setMargins(10.px, 0, 0, 0)
                }
            }

            val deleteIcon = ImageView(tagItem.context).apply {
                setImageResource(R.drawable.ic_close)
                layoutParams = RelativeLayout.LayoutParams(
                    20.px,
                    20.px
                ).apply {
                    addRule(RelativeLayout.RIGHT_OF, tagText.id)
                    addRule(RelativeLayout.CENTER_VERTICAL)
                    setMargins(20.px, 0, 10.px, 0)
                }
                id = View.generateViewId()
                setOnClickListener{
                    val builderRecipeDelete = AlertDialog.Builder(this@RecipeDetailActivity, R.style.MyDialogTheme)

                    builderRecipeDelete.setTitle(this.resources.getString(R.string.delete_tag))
                    builderRecipeDelete.setMessage(this.resources.getString(R.string.delete_tag_verification))

                    builderRecipeDelete.setNegativeButton(this.resources.getString(R.string.no)) { _, _ ->
                    }

                    builderRecipeDelete.setPositiveButton(this.resources.getString(R.string.yes)) { _, _ ->
                        deleteTag(tagItem, tag.name, recipe.id)
                    }

                    val dialogRecipeDelete: AlertDialog = builderRecipeDelete.create()

                    // Display the alert dialog on app interface
                    dialogRecipeDelete.show()
                    }
            }

            tagItem.addView(deleteIcon)
            tagItem.addView(tagText)
            list_tags.addView(tagItem)
        }
    }

    private fun showPopup(btn_edit: ImageView) {
        val popup = PopupMenu(this, btn_edit).apply {
            inflate(R.menu.popup_recipe_detail_edit)
            show()
        }
        popup.setOnMenuItemClickListener(this)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        var deleteBool = false
        if (item?.itemId == R.id.popupItem_delete){
            val builderRecipeDelete = AlertDialog.Builder(this@RecipeDetailActivity, R.style.MyDialogTheme)

            builderRecipeDelete.setTitle(this.resources.getString(R.string.delete_recipe))
            builderRecipeDelete.setMessage(this.resources.getString(R.string.delete_recipe_verification))

            builderRecipeDelete.setNegativeButton(this.resources.getString(R.string.no)) { _, _ ->
            }

            builderRecipeDelete.setPositiveButton(this.resources.getString(R.string.yes)) { _, _ ->
                deleteBool = deleteRecipe(item)
            }

            val dialogRecipeDelete: AlertDialog = builderRecipeDelete.create()

            // Display the alert dialog on app interface
            dialogRecipeDelete.show()
        } else {
            deleteBool = deleteRecipe(item)
        }
        return deleteBool
    }

    private fun deleteRecipe(item: MenuItem?) : Boolean {
        if (item?.itemId == R.id.popupItem_edit){
            val recipe = Gson().toJson(recipe)
            val intent = Intent(this, RecipeCreateActivity::class.java)
            intent.putExtra("recipe", recipe)
            startActivity(intent)
            return true
        }
        if (item?.itemId == R.id.popupItem_delete){
            viewModel.deleteRecipe(recipe)
            onBackPressed()
            return true
        }
        return false
    }

    // click on back button sends you back to the previous activity
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun createViewModel()  = runBlocking {
        launch(Dispatchers.IO) {
            viewModel = ViewModelProvider(this@RecipeDetailActivity).get(
                ViewModel::class.java
            )
        }
    }

    private fun deleteTag(
        tag: RelativeLayout,
        tagName : String,
        recipeId : Long)
    {
        // delete views
        if ((tag.parent as ViewGroup).childCount <= 1) {
            // Entfernen der Tag-Section und des nun unnötigen Dividers, wenn letztes Tag entfernt wird
            (section_tag.parent as ViewGroup).removeView(section_tag)
            (section_comment as ViewGroup).removeView(div_comment)

            if ((list_comments as ViewGroup).childCount < 1) {
                (section_ingredients as ViewGroup).removeView(div_preparation)
            }
        }
        (tag.parent as ViewGroup).removeView(tag)

        viewModel.deleteTag(tagName, recipeId)
    }
}