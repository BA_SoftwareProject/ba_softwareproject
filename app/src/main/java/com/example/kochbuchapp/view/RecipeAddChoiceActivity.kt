package com.example.kochbuchapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.kochbuchapp.R
import kotlinx.android.synthetic.main.activity_recipe_add_choice.*

class RecipeAddChoiceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_add_choice)

        // adds the back button to the actionbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_add_manually.setOnClickListener{
            startActivity(Intent(this, RecipeCreateActivity::class.java))
        }

        btn_add_per_url.setOnClickListener{
            startActivity(Intent(this, RecipeImportActivity::class.java))
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}