package com.example.kochbuchapp.view

import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import com.example.kochbuchapp.helper.dp
import com.example.kochbuchapp.view.recyclerViews.*
import com.example.kochbuchapp.data.model.RecipeModel
import com.example.kochbuchapp.data.model.TagModel
import com.example.kochbuchapp.data.repository.RecipeRepository
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import kotlinx.android.synthetic.main.nav_header.view.*
import kotlinx.android.synthetic.main.recipe_list_content.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

class RecipeListActivity : AppCompatActivity(), TagsOnStartDragListener {

    private lateinit var viewModel: ViewModel
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var toolbar: Toolbar
    private lateinit var navView: NavigationView
    private lateinit var searchView: SearchView
    private lateinit var tagList: MutableList<TagModel>
    private lateinit var recyclerView: RecyclerView
    private lateinit var tagsRecycleViewAdapter: SortTagsRecycleViewAdapter
    private lateinit var itemTouchHelperCallback: TagsItemTouchHelperCallback
    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var recipeList : MutableList<RecipeModel>
    private lateinit var repository: RecipeRepository
    private var searchword = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recipeList = mutableListOf()
        tagList = mutableListOf()
        repository= RecipeRepository(applicationContext)

        createViewModel()
        getCompleteRecipeList()

        viewModel.tagLiveData.observe(this, {
            tagList.clear()

            it.forEach{ t -> tagList.add(t) }
            tagList.sortBy{t -> t.listPosition}

            setRecyclerView()

            recyclerView = navView.getHeaderView(0).tag_filter_recyclerview
            tagsRecycleViewAdapter = SortTagsRecycleViewAdapter(tagList, this@RecipeListActivity, this)
            recyclerView.adapter = tagsRecycleViewAdapter
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.setHasFixedSize(true)

            // ItemTouchHelper for drag’n’drop functionality
            itemTouchHelperCallback = TagsItemTouchHelperCallback(tagsRecycleViewAdapter, repository)
            itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
            itemTouchHelper.attachToRecyclerView(recyclerView)
        })

        setContentView(R.layout.activity_recipe_list)

        // setting up toolbar
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // setting up Sidebar
        drawerLayout = findViewById(R.id.drawerLayout)
        navView = findViewById(R.id.navView)
        searchView = navView.getHeaderView(0).searchView

        // Search function for the SearchView in the sidebar
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                searchword = query!!
                setRecyclerView()
                return true
            }

            // unnecessary, but needs to be implemented
            override fun onQueryTextChange(query: String?): Boolean {
                searchword = query!!
                setRecyclerView()
                return true
            }
        })
        val searchCloseBtn = searchView.findViewById<ImageView>(androidx.appcompat.R.id.search_close_btn)
        searchCloseBtn.setOnClickListener {
            getCompleteRecipeList()
            searchView.setQuery("", false)
            searchView.clearFocus()
        }

        // Set Floating ActionButton
        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener {
            startActivity(Intent(this, RecipeAddChoiceActivity::class.java))
        }

        // SetUp RecipeList
        // Recipe Recyclerview
        val recView = RecipesRecycleViewAdapter(this, recipeList)
        recview_recipe_list.apply {
            adapter = recView
            layoutManager = LinearLayoutManager(this@RecipeListActivity)
        }

        // Swipe Functionality
        var deletedRecipe: RecipeModel?
        var swipeHelper : ItemTouchHelper? = null

        val swipeCallback =
            object : ItemTouchHelper.SimpleCallback(
                0,
                ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
            ) {
                // called on drag, not used here
                override fun onMove(
                    recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {return false}

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val position = viewHolder.adapterPosition
                    when (direction) {
                        ItemTouchHelper.LEFT -> {
                            deletedRecipe = recipeList[position]
                            viewModel.deleteRecipe(recipeList[position])
                            recipeList.removeAt(position)
                            recView.notifyItemRemoved(position)
                            Snackbar
                                .make(
                                    recview_recipe_list,
                                    "${getString(R.string.deleted)} ${deletedRecipe!!.title!!}",
                                    Snackbar.LENGTH_LONG
                                )
                                .setAction(getString(R.string.undo)) {
                                    recipeList.add(position, deletedRecipe!!)
                                    recView.notifyItemInserted(position)
                                    viewModel.saveRecipe(recipeList[position])
                                }
                                .show()
                        }
                        ItemTouchHelper.RIGHT -> {
                            val recipeToEdit = Gson().toJson(recipeList[position])
                            val intent = Intent(
                                recview_recipe_list.context,
                                RecipeCreateActivity::class.java
                            )
                            intent.putExtra("recipe", recipeToEdit)
                            startActivity(intent)
                            // return card into original position (android per default assumes deletion of listItem on swipe)
                            swipeHelper?.attachToRecyclerView(null)
                            swipeHelper?.attachToRecyclerView(recview_recipe_list)
                        }
                    }
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(ContextCompat.getColor(this@RecipeListActivity, R.color.delete))
                        .addSwipeLeftActionIcon(R.drawable.ic_delete_24)
                        .setSwipeLeftActionIconTint(ContextCompat.getColor(this@RecipeListActivity, R.color.colorDarkWhite))
                        .addSwipeLeftLabel(getString(R.string.delete))
                        .setSwipeLeftLabelColor(ContextCompat.getColor(this@RecipeListActivity, R.color.colorDarkWhite))
                        .setSwipeLeftLabelTypeface(ResourcesCompat.getFont(this@RecipeListActivity, R.font.montserrat))

                        .addSwipeRightBackgroundColor(ContextCompat.getColor(this@RecipeListActivity, R.color.colorBackground))
                        .addSwipeRightActionIcon(R.drawable.ic_edit_24)
                        .setSwipeRightActionIconTint(ContextCompat.getColor(this@RecipeListActivity, R.color.edit))
                        .addSwipeRightLabel(getString(R.string.edit))
                        .setSwipeRightLabelColor(ContextCompat.getColor(this@RecipeListActivity, R.color.colorTextDarkGray))
                        .setSwipeRightLabelTypeface(ResourcesCompat.getFont(this@RecipeListActivity, R.font.montserrat))

                        .setIconHorizontalMargin(TypedValue.COMPLEX_UNIT_DIP, resources.getDimension(R.dimen.app_padding_sides).toInt().dp)
                        .create()
                        .decorate()
                    super.onChildDraw(c,recyclerView,viewHolder,dX,dY,actionState,isCurrentlyActive)
                }
            }
        swipeHelper = ItemTouchHelper(swipeCallback)
        swipeHelper.attachToRecyclerView(recview_recipe_list)
    }

    private fun createViewModel()  = runBlocking {
        launch(Dispatchers.IO) {
            viewModel = ViewModelProvider(this@RecipeListActivity).get(
                ViewModel::class.java
            )
        }
    }

    fun closeDrawer(view: View) {
        drawerLayout.closeDrawer(GravityCompat.END)
    }

    fun openDrawer(view: View) {
        drawerLayout.openDrawer(GravityCompat.END)
    }

    // enables the drag handle
    override fun onStartDrag(holder: SortTagsRecycleViewAdapter.SortTagViewHolder) {
        itemTouchHelper.startDrag(holder)
    }

    private fun getCompleteRecipeList() {
        viewModel.recipeLiveData.observe(this, {
            recipeList = mutableListOf()
            it.forEach{ r ->
                recipeList.add(r.toRecipeModel())
            }
            setRecyclerView()
        })
    }

    private fun setRecyclerView() {
        val checkedTags = tagList.filter { it.isChecked == true}
        val recView = RecipesRecycleViewAdapter(this, filterRecipes(checkedTags, searchword))
        recview_recipe_list.apply {
            adapter = recView
            layoutManager = LinearLayoutManager(this@RecipeListActivity)
        }
    }

    private fun filterRecipes(
        tags: List<TagModel> = emptyList(),
        searchWord: String = ""
    ) : MutableList<RecipeModel> {
        var output = recipeList

        // if tags are given, change output to a list of recipes that contain those (without duplicates)
        if (tags.isNotEmpty()) {
            output = output.filter { recipe ->
                tags.forEach { tag ->
                        if(!recipe.tags.any{tag.tagId == it.tagId}) {
                        return@filter false
                    }
                }
                return@filter true
            }.toMutableList()
        }

        // if a search string is given split it up into a list of words (by spaces) and
        // 1. check if recipe-title contains any given substring or
        // 2. check if at least one of the ingredients names contain any given substring
        // 3. filter the output list by those two conditions
        if (searchWord.isNotBlank()) {
            val searchList = searchWord.toLowerCase(Locale.ROOT).split(" ")

            output = output.filter { recipe ->
                var flag = true

                searchList.forEach { word ->
                    if (!recipe.title!!.toLowerCase(Locale.ROOT).contains(word) &&
                        recipe.ingredients.none { it.name.toLowerCase(Locale.ROOT).contains(word)} &&
                        recipe.tags.none { it.name.toLowerCase(Locale.ROOT).contains((word))}
                    ) flag = false
                }

                return@filter flag
            }.toMutableList()
        }
        return output
    }
}