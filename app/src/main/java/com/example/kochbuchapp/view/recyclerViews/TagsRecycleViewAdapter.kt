package com.example.kochbuchapp.view.recyclerViews

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.model.TagModel
import kotlinx.android.synthetic.main.create_recipe_tags.view.*

class TagsRecycleViewAdapter(tagList: MutableList<TagModel>, context: Context, builder: AlertDialog.Builder): RecyclerView.Adapter<TagsRecycleViewAdapter.TagsViewHolder>() {

    private val mTagList = tagList
    private val mContext = context
    private val builderIngredient = builder

    // ViewHolder = one element of the RecyclerView
    class TagsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tagTextView: TextView = itemView.tag_tv
        val deleteImageView: ImageView = itemView.create_recipe_tag_delete_iv
    }

    // called, when the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.create_recipe_tags,
            parent, false
        )
        return TagsViewHolder(itemView)
    }

    // called, whenever an element of the RecyclerView is updated
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: TagsViewHolder, position: Int) {

        // setting values for each item
        holder.tagTextView.text = mTagList[position].name

        //removing items
        holder.deleteImageView.setOnClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_tag))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_tag_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mTagList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mTagList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_tag_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
        }

        holder.itemView.setOnLongClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_tag))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_tag_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mTagList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mTagList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_tag_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
            true
        }
    }

    // returns the size of the RecyclerView. Not used yet, but needs to be implemented
    override fun getItemCount(): Int {
        return mTagList.size
    }

    // adds an item. Called when a new instruction is added by clicking the green button
    fun addItem(tag: TagModel) {
        mTagList.add(tag)
        notifyItemChanged(mTagList.size - 1)
    }
}