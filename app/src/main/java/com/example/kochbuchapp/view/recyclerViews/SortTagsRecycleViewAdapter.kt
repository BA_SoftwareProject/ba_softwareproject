package com.example.kochbuchapp.view.recyclerViews

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.model.TagModel
import com.example.kochbuchapp.data.repository.RecipeRepository
import com.example.kochbuchapp.helper.ioThread
import kotlinx.android.synthetic.main.nav_header_tags.view.*
import kotlinx.coroutines.runBlocking
import java.util.*


// Adapter for the RecyclerView that contains the tags with their checkboxes in the sidebar
class SortTagsRecycleViewAdapter(tagList: MutableList<TagModel>, context: Context, dragListener: TagsOnStartDragListener): RecyclerView.Adapter<SortTagsRecycleViewAdapter.SortTagViewHolder>(), ItemTouchHelperAdapter {
    val mTagList = tagList
    val posList : MutableList<String> = mutableListOf()
    private val mContext = context
    private val mDragListener = dragListener
    private val repository : RecipeRepository = RecipeRepository(mContext)

    // ViewHolder = one element of the RecyclerView
    class SortTagViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),
        ItemTouchHelperViewHolder {
        val imageView: ImageView = itemView.nav_header_iv
        val checkBox: CheckBox = itemView.nav_header_cb

        // changes color of itemView to highlight the dragged item -> optical feedback
        override fun onItemSelected() {
            itemView.setBackgroundColor(imageView.context.getColor(R.color.colorPrimaryDark))
        }

        // changes color of dropped itemView back to primaryColor
        override fun onItemClear() {
            itemView.setBackgroundColor(imageView.context.getColor(R.color.colorAccent))
        }
    }

    // called, when the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortTagViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.nav_header_tags,
            parent, false
        )
        return SortTagViewHolder(itemView)
    }

    // called, whenever an element of the RecyclerView is updated
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: SortTagViewHolder, position: Int) {

        // setting properties for each element of the RecyclerView
        val currentItem = mTagList[position]
        holder.imageView.setImageResource(R.drawable.ic_drag_handle)
        holder.checkBox.text = currentItem.name
        holder.checkBox.isChecked = currentItem.isChecked!!

        // Set positionList
        posList.add(currentItem.name)

        // colorStateList stores color for checked/unchecked state of checkbox
        val colorStateList = ColorStateList(
            arrayOf(
                intArrayOf(android.R.attr.state_checked),
                intArrayOf(-android.R.attr.state_checked)
            ),
            intArrayOf(
                holder.imageView.context.getColor(R.color.colorTextDarkGray),
                holder.imageView.context.getColor(R.color.colorDarkWhite)
            )
        )
        holder.checkBox.buttonTintList = colorStateList
        holder.checkBox.setTextColor(colorStateList)

        if(!holder.checkBox.isChecked) {
            holder.imageView.setColorFilter(holder.imageView.context.getColor(R.color.colorDarkWhite))
        }


        // setting OnTouchListener for imageView, so it can work as a handle
        holder.imageView.setOnTouchListener {_, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN
            ) {
                // Notify ItemTouchHelper to start dragging
                mDragListener.onStartDrag(holder)
            }
            false
        }

        // OnClickListener when checkboxes are checked/unchecked
        holder.checkBox.setOnClickListener {
            fun updateClickedTag(checked : Boolean) {
                val clickedTag = repository.getTagByName(holder.checkBox.text.toString())
                clickedTag?.let { repository.saveTag(it.apply { isChecked = checked }, null) }
            }

            if (holder.checkBox.isChecked) {
                // change the color of the recyclerview elements to dark gray
                holder.imageView.setColorFilter(holder.checkBox.context.getColor(R.color.colorTextDarkGray))

                //Toast.makeText(mContext, "${holder.checkBox.text} checked", Toast.LENGTH_SHORT).show()
                runBlocking { ioThread { updateClickedTag(true) } }
            }
            else {
                // change the color of the recyclerview elements to white
                holder.imageView.setColorFilter(holder.imageView.context.getColor(R.color.colorDarkWhite))

                //Toast.makeText(mContext, "${holder.checkBox.text} unchecked", Toast.LENGTH_SHORT).show()
                runBlocking { ioThread { updateClickedTag(false) } }
            }
        }
    }

    // returns the size of the RecyclerView. Not used yet, but needs to be implemented
    override fun getItemCount(): Int {
        return mTagList.size
    }

    // swaps the position of the moved item
    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(posList, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
    }
}

interface TagsOnStartDragListener {
    fun onStartDrag(holder: SortTagsRecycleViewAdapter.SortTagViewHolder)
}