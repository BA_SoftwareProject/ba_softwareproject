package com.example.kochbuchapp.view

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kochbuchapp.R
import com.example.kochbuchapp.view.recyclerViews.*
import kotlinx.android.synthetic.main.activity_recipe_create.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import com.example.kochbuchapp.data.model.*
import com.google.gson.Gson
import java.io.FileOutputStream
import kotlin.random.Random
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView


class RecipeCreateActivity : AppCompatActivity(), IngredientsOnStartDragListener, InstructionsOnStartDragListener {

    private lateinit var viewModel: ViewModel
    private var recipe: RecipeModel? = null

    //imageFile
    private lateinit var imageFile: File
    private lateinit var uri: Uri
    private var imageGalleryPath: String? = null
    private var pictureDir: File? = null
    private var isNewImageSet: Boolean = false
    private var imageID: Long = 0
    private var imageName: String = ""
    private var oGImgReference: String = ""
    private var oGTitle: String = ""

    // RecyclerViews
    private lateinit var decorator: DividerItemDecoration
    private lateinit var builderIngredient: AlertDialog.Builder

    //ingredients
    private var ingredientList: MutableList<IngredientModel> = ArrayList()
    private lateinit var unitSpinnerSelection: String
    private lateinit var ingredientRecyclerView: RecyclerView
    private lateinit var ingredientsRecycleViewAdapter: IngredientsRecycleViewAdapter
    private lateinit var ingredientItemTouchHelper: ItemTouchHelper
    private lateinit var ingredientItemTouchHelperCallback: IngredientsItemTouchHelperCallback

    //description
    private var instructionList: MutableList<String> = ArrayList()
    private lateinit var instructionRecyclerView: RecyclerView
    private lateinit var instructionRecycleViewAdapter: InstructionsRecycleViewAdapter
    private lateinit var instructionsItemTouchHelper: ItemTouchHelper
    private lateinit var instructionsItemTouchHelperCallback: InstructionsItemTouchHelperCallback

    //tag
    private var tagList: MutableList<TagModel> = ArrayList()
    private lateinit var tagRecyclerView: RecyclerView
    private lateinit var tagRecycleViewAdapter: TagsRecycleViewAdapter
    private lateinit var tagsuggestions: MutableList<String>

    //comment
    private var commentList: MutableList<String> = ArrayList()
    private lateinit var commentRecyclerView: RecyclerView
    private lateinit var commentRecycleViewAdapter: CommentsRecycleViewAdapter

    private var idBool: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        createViewModel()

        recipe = Gson().fromJson(
            intent.getStringExtra("recipe"),
            RecipeModel::class.java
        )

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_create)

        // adds the back button to the actionbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = this.resources.getString(R.string.create_recipe)

        // check if recipe is imported or not
        // Get the recipe given as parameter (see clickListener in RecipeListActivity
        if (recipe != null) {
            supportActionBar?.title = this.resources.getString(R.string.edit_recipe)
            idBool = true
            editText_RecipeName.setText(recipe!!.title)
            oGTitle = recipe!!.title!!
            //image
            recipe!!.image?.let {
                if (it.hasImage) {
                    oGImgReference = it.reference
                    Glide.with(this).load(oGImgReference).into(image_view)
                    if (!it.isLocal) {
                        isNewImageSet = true
                    }
                }
            }

            editTextNumber_PrepTime.setText(recipe!!.preparationTime.toString())
            editTextNumber_CookingTime.setText(recipe!!.cookingTime.toString())
            editTextNumber_Portions.setText(recipe!!.portions.toString())

            ingredientList = recipe!!.ingredients.toMutableList()
            instructionList = recipe!!.instructions.toMutableList()
            tagList = recipe!!.tags.toMutableList()
            commentList = recipe!!.comments.toMutableList()
            if (recipe!!.comments.size == 1 && recipe!!.comments[0] == "") {
                commentList.removeAt(0)
            }
        }


        // ##### RecyclerViews #####
        builderIngredient = AlertDialog.Builder(this, R.style.MyDialogTheme)
        decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)

        // RecyclerView for ingredients
        ingredientRecyclerView = ingredients_recyclerview
        ingredientsRecycleViewAdapter = IngredientsRecycleViewAdapter(
            ingredientList,
            applicationContext,
            this,
            builderIngredient
        )
        ingredientRecyclerView.adapter = ingredientsRecycleViewAdapter
        ingredientRecyclerView.layoutManager = LinearLayoutManager(this)

        ingredientItemTouchHelperCallback =
            IngredientsItemTouchHelperCallback(ingredientsRecycleViewAdapter)
        ingredientItemTouchHelper = ItemTouchHelper(ingredientItemTouchHelperCallback)
        ingredientItemTouchHelper.attachToRecyclerView(ingredientRecyclerView)


        // RecyclerView for instructions
        instructionRecyclerView = instructions_recyclerview
        instructionRecycleViewAdapter = InstructionsRecycleViewAdapter(
            instructionList,
            applicationContext,
            this,
            builderIngredient
        )
        instructionRecyclerView.adapter = instructionRecycleViewAdapter
        instructionRecyclerView.layoutManager = LinearLayoutManager(this)
        instructionRecyclerView.addItemDecoration(VerticalSpaceItemDecoration(24))

        instructionsItemTouchHelperCallback =
            InstructionsItemTouchHelperCallback(instructionRecycleViewAdapter)
        instructionsItemTouchHelper = ItemTouchHelper(instructionsItemTouchHelperCallback)
        instructionsItemTouchHelper.attachToRecyclerView(instructionRecyclerView)


        // RecyclerView for tags
        tagRecyclerView = tags_recyclerview
        tagRecycleViewAdapter =
            TagsRecycleViewAdapter(tagList, applicationContext, builderIngredient)
        tagRecyclerView.adapter = tagRecycleViewAdapter
        tagRecyclerView.layoutManager = LinearLayoutManager(this)
        tagRecyclerView.addItemDecoration(VerticalSpaceItemDecoration(32))

        // RecyclerView for comments
        commentRecyclerView = comments_recyclerview
        commentRecycleViewAdapter =
            CommentsRecycleViewAdapter(commentList, applicationContext, builderIngredient)
        commentRecyclerView.adapter = commentRecycleViewAdapter
        commentRecyclerView.layoutManager = LinearLayoutManager(this)
        commentRecyclerView.addItemDecoration(VerticalSpaceItemDecoration(24))

        // ##### ImageButtons for adding elements to lists #####

        // adding ingredients
        imageButton_AddIngredient.setOnClickListener {
            if ((editText_IngredientName.text.isNotEmpty() && editText_IngredientName.text.isNotBlank())) {
                val ingredient: IngredientModel
                if (editText_IngredientAmount.text.isNotEmpty()) {
                    val ingredientAmount: Double =
                        editText_IngredientAmount.text.toString().toDouble()
                    ingredient = IngredientModel(
                        1,
                        1,
                        editText_IngredientName.text.toString(),
                        unitSpinnerSelection,
                        ingredientAmount,
                        null
                    )
                } else {
                    ingredient = IngredientModel(
                        1,
                        1,
                        editText_IngredientName.text.toString(),
                        unitSpinnerSelection,
                        0.0,
                        null
                    )
                }
                ingredientsRecycleViewAdapter.addItem(ingredient)

                editText_IngredientAmount.setText("")
                editText_IngredientName.setText("")
            }
        }

        // adding instructions
        imageButton_Description.setOnClickListener {
            if (editTextTextMultiLine_Description.text.isNotEmpty() && editTextTextMultiLine_Description.text.isNotBlank()) {
                instructionRecyclerView.invalidateItemDecorations()
                instructionRecycleViewAdapter.addItem(editTextTextMultiLine_Description.text.toString())
                editTextTextMultiLine_Description.setText("")
            }
        }

        // adding tags
        imageButton_AddTag.setOnClickListener {
            if(autoTextView.text.isNotEmpty() && autoTextView.text.trim().isNotBlank()) {
                tagRecyclerView.invalidateItemDecorations()
                tagRecycleViewAdapter.addItem(
                    TagModel(
                        tagId = 0L,
                        name = autoTextView.text.toString(),
                        isChecked = false,
                        listPosition = null
                    )
                )
                autoTextView.text.clear()
            }
        }

        // adding comments
        imageButton_Comment.setOnClickListener {
            if (editTextTextMultiLine_Comment.text.isNotEmpty() && editTextTextMultiLine_Comment.text.isNotBlank()) {
                commentRecyclerView.invalidateItemDecorations()
                commentRecycleViewAdapter.addItem(editTextTextMultiLine_Comment.text.toString())
                editTextTextMultiLine_Comment.setText("")
            }
        }


        //+++ Dropdown for the units
        // access the items of the list (They are saved in the values/strings.xml)
        val units = resources.getStringArray(R.array.unitArray)

        // access the spinner
        if (unit_Spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, units
            )
            unit_Spinner.adapter = adapter

            unit_Spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    unitSpinnerSelection = unit_Spinner.getItemAtPosition(position).toString()

                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }
        }

        // save Button
        button_save.setOnClickListener {
            if (editText_RecipeName.text.isEmpty() || editTextNumber_PrepTime.text.isEmpty() ||
                editTextNumber_CookingTime.text.isEmpty() || editTextNumber_Portions.text.isEmpty()) {
                Toast.makeText(
                    applicationContext,
                    this.resources.getString(R.string.not_everything_filled_out),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                if (recipe == null) {
                    recipe = RecipeModel()
                }
                recipe!!.title = editText_RecipeName.text.toString()
                recipe!!.preparationTime = editTextNumber_PrepTime.text.toString().toInt()
                recipe!!.cookingTime = editTextNumber_CookingTime.text.toString().toInt()
                recipe!!.portions = editTextNumber_Portions.text.toString().toInt()
                recipe!!.ingredients = ingredientList
                recipe!!.instructions = instructionList
                recipe!!.tags = tagList
                recipe!!.comments = commentList

                if (!idBool) {
                    recipe!!.id = Random.nextLong()
                }

                // ## IMAGE ##
                // generate new id if no images has been saved yet for this recipe
                imageID = if (isNewImageSet) {
                    Random.nextLong()
                } else {
                    recipe!!.image?.imageId?: 0
                }

                // create the name of the image
                if (recipe!!.title!!.length > 14) {
                    imageName = recipe!!.title!!.substring(0, 15) + "_" + imageID
                    imageName = imageName.replace(" ", "_")
                } else {
                    imageName = recipe!!.title!! + "_" + imageID
                    imageName = imageName.replace(" ", "_")
                }

                // save the image
                if (isNewImageSet || ((editText_RecipeName.text.toString() != oGTitle && oGTitle != "") && recipe!!.image?.hasImage!!)) {
                    // if a new image has been assigned to the recipe or the title has been changed
                    pictureDir =
                        applicationContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                    runBlocking {
                        GlobalScope.launch(Dispatchers.IO) {
                            if (imageGalleryPath != null) {
                                // new image picked from gallery
                                saveImage(imageGalleryPath!!, pictureDir!!, imageName)
                            } else {
                                // image from ChefKoch or phone storage
                                saveImage(oGImgReference, pictureDir!!, imageName)
                            }
                            if (File(oGImgReference).exists()) {
                                File(oGImgReference).delete()
                            }
                        }
                    }
                    recipe!!.image = RecipeImageModel(
                        imageID,
                        hasImage = true,
                        isLocal = true,
                        pictureDir!!.absolutePath + "/img_$imageName.jpg"
                        //reference: "file:///storage/emulated/0/Android/data/com.example.kochbuch_app/files/Pictures/img_$imageID.jpg"
                    )
                } else {
                    if(oGTitle == "") {
                        recipe!!.image = RecipeImageModel(
                            imageId = 0,
                            hasImage = false,
                            isLocal = false,
                            reference = ""
                        )
                    }
                }

                viewModel.viewModelScope.launch(Dispatchers.IO) {
                    viewModel.saveRecipe(recipe!!)
                }
                startActivity(Intent(this, RecipeListActivity::class.java))
            }
        }

            //+++ ImageView for uploading pictures for the recipe
            image_view.setOnClickListener {
                //check runtime permission
                CropImage.startPickImageActivity(this)
            }

            //+++ Dropdown for the tags
            // access the items of the list (They are saved in the values/strings.xml)
            tagsuggestions = mutableListOf()
            viewModel.tagLiveData.observe(this, {
                it.forEach { t ->
                    tagsuggestions.add(t.name)
                }
            })
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1, tagsuggestions
            )
            autoTextView.setAdapter(adapter)

    }


    // activates the drawer next to a recipe
    override fun ingredientsOnStartDrag(holder: IngredientsRecycleViewAdapter.IngredientsViewHolder) {
        ingredientItemTouchHelper.startDrag(holder)
    }

    override fun instructionsOnStartDrag(holder: InstructionsRecycleViewAdapter.InstructionsViewHolder) {
        instructionsItemTouchHelper.startDrag(holder)
    }


    companion object {
        //Permission code
        private const val PERMISSION_CODE = 1001
    }

    //handle result of picked image
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val imageUri: Uri = CropImage.getPickImageResultUri(this, data)
            imageGalleryPath = data?.data.toString()
            image_view.setImageURI(Uri.parse(imageGalleryPath))
            isNewImageSet = true
            val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                uri = imageUri
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                startCrop(imageUri)
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result: CropImage.ActivityResult = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                image_view.setImageURI(result.uri)
                imageGalleryPath = result.uri.toString()
            }
        }
    }

    private fun startCrop(imageUri: Uri) {
        CropImage.activity(imageUri)
            .setFixAspectRatio(true)
            .setAspectRatio(7, 7)
            .setGuidelines(CropImageView.Guidelines.ON)
            .setMultiTouchEnabled(true)
            .start(this)
    }


    // click on back button sends you back to the previous activity
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun createViewModel()  = runBlocking {
        launch(Dispatchers.IO) {
            viewModel = ViewModelProvider(this@RecipeCreateActivity).get(
                ViewModel::class.java
            )
        }
    }

    private fun saveImage(resource: String, dir: File, name: String) {
        val imgBitmap = Glide.with(this)
            .asBitmap()
            .load(resource)
            .submit()
            .get()

        try {
            imageFile = File(dir, "img_$name.jpg")
            val outputStream = FileOutputStream(imageFile)
            imgBitmap.compress(Bitmap.CompressFormat.JPEG, 85, outputStream)
            outputStream.flush()
            outputStream.close()

        } catch (e: Exception) {
            Log.i("ImageSaver", "Failed to save image: "+ e.message)
        }
    }
}
