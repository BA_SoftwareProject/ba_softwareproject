package com.example.kochbuchapp.view.recyclerViews

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import kotlinx.android.synthetic.main.create_recipe_instructions.view.*
import java.util.*

class InstructionsRecycleViewAdapter(
    instructionsList: MutableList<String>,
    context: Context,
    dragListener: InstructionsOnStartDragListener,
    builder: AlertDialog.Builder
): RecyclerView.Adapter<InstructionsRecycleViewAdapter.InstructionsViewHolder>(),
    ItemTouchHelperAdapter {

    private val mInstructionList = instructionsList
    private val mContext = context
    private val mDragListener = dragListener
    private val builderIngredient = builder

    // ViewHolder = one element of the RecyclerView
    class InstructionsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),
        ItemTouchHelperViewHolder {
        val drawerImageView: ImageView = itemView.create_recipe_instructions_draghandle_iv
        val instructionTextView: TextView = itemView.instruction_tv
        val deleteImageView: ImageView = itemView.create_recipe_instructions_delete_iv

        // changes color of itemView to highlight the dragged item -> optical feedback
        override fun onItemSelected() {
            itemView.setBackgroundColor(
                ResourcesCompat.getColor(
                    itemView.resources,
                    R.color.colorBackgroundDrag,
                    null
                )
            )
        }

        // changes color of dropped itemView back to primaryColor
        override fun onItemClear() {
            itemView.setBackgroundColor(
                ResourcesCompat.getColor(
                    itemView.resources,
                    R.color.colorBackground,
                    null
                )
            )
        }
    }

    // called, when the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstructionsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.create_recipe_instructions,
            parent, false
        )
        return InstructionsViewHolder(itemView)
    }

    // called, whenever an element of the RecyclerView is updated
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: InstructionsViewHolder, position: Int) {

        // setting values for each item
        holder.instructionTextView.text = mInstructionList[position]


        // setting OnTouchListener for imageView, so it can work as a handle
        holder.drawerImageView.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN
            ) {
                // Notify ItemTouchHelper to start dragging
                mDragListener.instructionsOnStartDrag(holder)
            }
            false
        }

        //removing items
        holder.deleteImageView.setOnClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_instruction))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_instruction_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mInstructionList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mInstructionList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_ingredient_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
        }

        holder.itemView.setOnLongClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_instruction))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_instruction_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mInstructionList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mInstructionList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_ingredient_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
            true
        }

        holder.drawerImageView.setOnClickListener{}
    }

    // returns the size of the RecyclerView. Not used yet, but needs to be implemented
    override fun getItemCount(): Int {
        return mInstructionList.size
    }

    // swaps the position of the moved item
    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(mInstructionList, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)

    }

    // adds an item. Called when a new instruction is added by clicking the green button
    fun addItem(instruction: String) {
        mInstructionList.add(instruction)
        notifyItemChanged(mInstructionList.size - 1)
    }
}

interface InstructionsOnStartDragListener {
    fun instructionsOnStartDrag(holder: InstructionsRecycleViewAdapter.InstructionsViewHolder)
}