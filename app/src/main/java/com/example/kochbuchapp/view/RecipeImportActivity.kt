package com.example.kochbuchapp.view

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import com.example.kochbuchapp.data.model.RecipeModel
import kotlinx.coroutines.runBlocking
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.repository.RecipeRepository
import com.example.kochbuchapp.helper.ApiException
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_recipe_import.*

class RecipeImportActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_import)

        // adds the back button to the actionbar
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Slightly move view upwards, when keyboard appears
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        val repository = RecipeRepository(application)

        btn_add.setOnClickListener {
            val url = inp_URLInput.text.toString()
            val recipeId : String? = repository.getIdFromUrl(url)

            if (recipeId == null) {
                Toast.makeText(applicationContext,R.string.recipeCreateChoice_urlFormatError, Toast.LENGTH_LONG).show()
            } else {
                if(!checkNetwork(applicationContext)) {
                    Toast.makeText(applicationContext,R.string.recipeCreateChoice_NoInternet, Toast.LENGTH_LONG).show()
                } else {
                    var recipeModel: RecipeModel? = null
                        runBlocking {
                            try {
                                recipeModel = repository.findRecipe(recipeId, applicationContext)
                            } catch (e : ApiException) {
                                Toast.makeText(applicationContext,e.message, Toast.LENGTH_LONG).show()
                            }
                    }
                    recipeModel?.let {
                        val recipe = Gson().toJson(it)
//                        val intent = Intent(this, RecipeDetailActivity::class.java)
                        val intent = Intent(this, RecipeCreateActivity::class.java)
                        intent.putExtra("recipe", recipe)
//                        intent.putExtra("recipe", it.id)
                        startActivity(intent)
                    }
                }
            }
        }
    }

    // check if an Internet connection is available
    private fun checkNetwork(context: Context): Boolean {
        val connectivityManager = getSystemService(context, ConnectivityManager::class.java)!!
        val linkProperties = connectivityManager.getLinkProperties(connectivityManager.activeNetwork)
        return (linkProperties != null)
    }

    // click on back button sends you back to the previous activity
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}