package com.example.kochbuchapp.view.recyclerViews

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.model.RecipeModel
import com.example.kochbuchapp.view.RecipeDetailActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.recipe_list_listitem.view.*

class RecipesRecycleViewAdapter(
    val context : Context,
    private val recipeList: MutableList<RecipeModel>
): RecyclerView.Adapter<RecipesRecycleViewAdapter.RecipeViewHolder>() {

    class RecipeViewHolder(listItem: View) : RecyclerView.ViewHolder(listItem) {
        val recipeCardThumbnail: ImageView = listItem.recipeCard_thumbnail
        val recipeCardTitle: TextView = listItem.recipeCard_title
        val recipeCard : CardView = listItem.recipeCard_outer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecipeViewHolder {
        val listItem = LayoutInflater
            .from(this.context)
            .inflate(
                R.layout.recipe_list_listitem,
                parent,
                false
            )

        return RecipeViewHolder(listItem)
    }

    override fun onBindViewHolder(
        holder: RecipeViewHolder,
        position: Int
    ) {
        holder.recipeCardTitle.text = recipeList[position].title
        Glide.with(context).load(recipeList[position].image?.reference).placeholder(R.drawable.placeholder600x400).into(holder.recipeCardThumbnail)

        holder.recipeCard.setOnClickListener {
            val recipeJSON = Gson().toJson(recipeList[position])
            val intent = Intent(context, RecipeDetailActivity::class.java)
            intent.putExtra("recipe", recipeJSON)
            startActivity(context, intent, null)
        }
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }
}