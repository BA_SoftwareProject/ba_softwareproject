package com.example.kochbuchapp.view.recyclerViews

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import kotlinx.android.synthetic.main.create_recipe_comments.view.*

class CommentsRecycleViewAdapter(commentList: MutableList<String>, context: Context, builder: AlertDialog.Builder): RecyclerView.Adapter<CommentsRecycleViewAdapter.CommentsViewHolder>() {

    private val mCommentList = commentList
    private val mContext = context
    private val builderIngredient = builder

    // ViewHolder = one element of the RecyclerView
    class CommentsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val commentTextView: TextView = itemView.comment_tv
        val deleteImageView: ImageView = itemView.create_recipe_comments_delete_iv
    }

    // called, when the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.create_recipe_comments,
            parent, false
        )
        return CommentsViewHolder(itemView)
    }

    // called, whenever an element of the RecyclerView is updated
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {

        // setting values for each item
        holder.commentTextView.text = mCommentList[position]


        //removing items
        holder.deleteImageView.setOnClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_comment))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_comment_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mCommentList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mCommentList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_comment_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
        }

        holder.itemView.setOnLongClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_comment))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_comment_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mCommentList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mCommentList.size)
                Toast.makeText(
                    mContext,
                    it.resources.getString(R.string.delete_comment_validation),
                    Toast.LENGTH_SHORT
                ).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
            true
        }
    }

    // returns the size of the RecyclerView. Not used yet, but needs to be implemented
    override fun getItemCount(): Int {
        return mCommentList.size
    }

    // adds an item. Called when a new instruction is added by clicking the green button
    fun addItem(comment: String) {
        mCommentList.add(comment)
        notifyItemChanged(mCommentList.size - 1)
    }
}