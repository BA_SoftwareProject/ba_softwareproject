package com.example.kochbuchapp.view.recyclerViews

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.model.IngredientModel
import kotlinx.android.synthetic.main.create_recipe_ingredients.view.*
import java.util.*

class IngredientsRecycleViewAdapter(ingredientsList: MutableList<IngredientModel>, context: Context, dragListener: IngredientsOnStartDragListener, builder: AlertDialog.Builder): RecyclerView.Adapter<IngredientsRecycleViewAdapter.IngredientsViewHolder>(),
    ItemTouchHelperAdapter {

    private val mIngredientsList = ingredientsList
    private val mContext = context
    private val mDragListener = dragListener
    private val builderIngredient = builder

    // ViewHolder = one element of the RecyclerView
    class IngredientsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView),
        ItemTouchHelperViewHolder {
        val drawerImageView: ImageView = itemView.create_recipe_ingredients_draghandle_iv
        val ingredientsTextView: TextView = itemView.ingredients_tv
        val deleteImageView: ImageView = itemView.create_recipe_ingredients_delete_iv

        // changes color of itemView to highlight the dragged item -> optical feedback
        override fun onItemSelected() {
            itemView.setBackgroundColor(ResourcesCompat.getColor(itemView.resources, R.color.colorBackgroundDrag, null))
        }

        // changes color of dropped itemView back to primaryColor
        override fun onItemClear() {
            itemView.setBackgroundColor(ResourcesCompat.getColor(itemView.resources, R.color.colorBackground, null))
        }
    }

    // called, when the RecyclerView is created
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.create_recipe_ingredients,
            parent, false
        )
        return IngredientsViewHolder(itemView)
    }

    // called, whenever an element of the RecyclerView is updated
    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {

        // setting values for each item
        val currentItem = mIngredientsList[position]
        var displayedString: String

        displayedString = if(currentItem.amount % 1.0 == 0.0) {
            if(currentItem.amount != 0.0) {
                currentItem.amount.toInt().toString() + " "
            } else ""
        } else {
            currentItem.amount.toString() + " "
        }
        if(currentItem.unit != "") {
            displayedString += currentItem.unit + "  "
        }
        displayedString += currentItem.name

        holder.ingredientsTextView.text = displayedString

        // setting OnTouchListener for imageView, so it can work as a handle
        holder.drawerImageView.setOnTouchListener {_, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN
            ) {
                // Notify ItemTouchHelper to start dragging
                mDragListener.ingredientsOnStartDrag(holder)
            }
            false
        }

        //removing items
        holder.deleteImageView.setOnClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_ingredient))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_ingredient_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mIngredientsList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mIngredientsList.size)
                Toast.makeText(mContext, it.resources.getString(R.string.delete_ingredient_validation), Toast.LENGTH_SHORT).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
        }

        holder.itemView.setOnLongClickListener {
            builderIngredient.setTitle(it.resources.getString(R.string.delete_ingredient))
            builderIngredient.setMessage(it.resources.getString(R.string.delete_ingredient_verification))

            builderIngredient.setNegativeButton(it.resources.getString(R.string.no)) { _, _ ->
            }

            builderIngredient.setPositiveButton(it.resources.getString(R.string.yes)) { _, _ ->
                // Delete items of the array
                mIngredientsList.removeAt(holder.adapterPosition)
                notifyItemRemoved(holder.adapterPosition)
                notifyItemRangeChanged(holder.adapterPosition, mIngredientsList.size)
                Toast.makeText(mContext, it.resources.getString(R.string.delete_ingredient_validation), Toast.LENGTH_SHORT).show()
            }

            val dialogIngredient: AlertDialog = builderIngredient.create()

            // Display the alert dialog on app interface
            dialogIngredient.show()
            true
        }

        holder.drawerImageView.setOnClickListener{}
    }

    // returns the size of the RecyclerView. Not used yet, but needs to be implemented
    override fun getItemCount(): Int {
        return mIngredientsList.size
    }

    // swaps the position of the moved item
    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(mIngredientsList, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)

    }

    // adds an item. Called when a new ingredient is added by clicking the green button
    fun addItem(ingredient: IngredientModel) {
        mIngredientsList.add(ingredient)
        notifyItemChanged(mIngredientsList.size - 1)
    }
}

interface IngredientsOnStartDragListener {
    fun ingredientsOnStartDrag(holder: IngredientsRecycleViewAdapter.IngredientsViewHolder)
}