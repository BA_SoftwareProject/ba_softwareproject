package com.example.kochbuchapp.view.recyclerViews

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.kochbuchapp.data.repository.RecipeRepository
import com.example.kochbuchapp.helper.ioThread
import kotlinx.coroutines.runBlocking

class TagsItemTouchHelperCallback(
    tagsRecycleViewAdapter: SortTagsRecycleViewAdapter,
    repository: RecipeRepository
): ItemTouchHelper.Callback() {
    private val mTagsRecycleViewAdapter = tagsRecycleViewAdapter
    private val mRepository = repository

    // gets the movement flags for drag'n'drop: up and down movement
    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG, ItemTouchHelper.UP or ItemTouchHelper.DOWN)
    }

    override fun isLongPressDragEnabled(): Boolean {
        return false
    }

    // called whenever an item is swapped with another
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        mTagsRecycleViewAdapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)
        return true
    }

    // called whenever an item is swiped to the side. Not used here, but needs to be implemented
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    }

    // called whenever an item is starting to get dragged -> changes color to highlight dragged item and updates the tagList
    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            val itemViewHolder = viewHolder as SortTagsRecycleViewAdapter.SortTagViewHolder
            itemViewHolder.onItemSelected()
        }
        super.onSelectedChanged(viewHolder, actionState)
    }

    // called whenever an item is "dropped" -> changes color back to normal and updates the tag table in database
    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        val itemViewHolder = viewHolder as SortTagsRecycleViewAdapter.SortTagViewHolder
        itemViewHolder.onItemClear()

        val tagList = mTagsRecycleViewAdapter.mTagList.map{ it }.toMutableList()
        tagList.forEach {
            it.listPosition = mTagsRecycleViewAdapter.posList.indexOf(it.name)
        }
        runBlocking { ioThread { mRepository.updateTagPositions(tagList) } }
    }
}

interface ItemTouchHelperAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int)
}

interface ItemTouchHelperViewHolder {
    fun onItemSelected()
    fun onItemClear()
}
