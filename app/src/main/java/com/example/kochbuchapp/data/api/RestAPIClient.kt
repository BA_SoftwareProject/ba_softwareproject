package com.example.kochbuchapp.data.api

import android.content.Context
import com.example.kochbuchapp.R
import com.example.kochbuchapp.data.model.*
import com.example.kochbuchapp.helper.ApiException
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

class RestAPIClient {

    private val chefKochApi : ChefKochApi

    // Initialising Retrofit-Client
    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.chefkoch.de")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        chefKochApi = retrofit.create(ChefKochApi::class.java)
    }

    // Function to get a RecipeModel instance with the given recipe_id
    suspend fun findRecipe(recipe_id: String, context: Context?): RecipeModel? {
        var apiRecipe : RecipeModel? = null

        try {
            val response = chefKochApi.getRecipe(recipe_id)

            if (!response.isSuccessful) // Communication with ChefKoch failed
                context?.let { throw ApiException(it.resources.getString(R.string.recipeCreateChoice_ApiNotAvaiable)) }

            if (response.body() == null) // No recipe found
                context?.let { throw ApiException(it.resources.getString(R.string.recipeCreateChoice_urlError)) }

            apiRecipe = convertRecipeModel(response.body()!!)
        } catch (e: Exception) {
            context?.let { throw ApiException(e.message!!) }
        }

        return apiRecipe
    }

    // Function to convert an ApiRecipeModel instance to a RecipeModel instance
    private fun convertRecipeModel(apiRecipeModel: ApiRecipeModel): RecipeModel {

        var imageReference = ""
        if (apiRecipeModel.hasImage) {
            val stringList = apiRecipeModel.siteUrl.split("/")
            val urlName = stringList[5].replace(".html", "")
            imageReference = "https://img.chefkoch-cdn.de/rezepte/${apiRecipeModel.id}/bilder/${apiRecipeModel.previewImageId}/crop-600x400/$urlName.jpg"
        }
        val image = RecipeImageModel(
                0L,
                apiRecipeModel.hasImage,
                false,
                imageReference
        )

        val instructionsString = apiRecipeModel.instructions.replace("\r", "")
        val preInstructionList = instructionsString.split("\n")
        val instructionList = mutableListOf<String>()
        preInstructionList.forEach { if (it.count() > 2) instructionList.add(it) }

        val ingredientsList : MutableList<IngredientModel> = mutableListOf()
        apiRecipeModel.ingredientGroups.forEach { ingredientGroup ->
            ingredientGroup.ingredients.forEach { ing ->
                ingredientsList.add(
                    IngredientModel(
                        0L,
                        recipeId = null,
                        name = ing.name,
                        unit = ing.unit,
                        amount = ing.amount,
                        productGroup = ing.productGroup
                    )
                )
            }
        }

        val tagsList : MutableList<TagModel> = mutableListOf()
        val preTagsList = apiRecipeModel.fullTags.toSet().toMutableList().filter { tag : ApiTag -> tag.name.isNotBlank() }
        preTagsList.forEach {
            tagsList.add(
                TagModel(
                tagId = 0L,
                name = it.name,
                listPosition = null,
                isChecked = false,
            )
            )
        }

        return RecipeModel(
                0L,
                apiRecipeModel.title,
                apiRecipeModel.subtitle,
                image,
                apiRecipeModel.createdAt,
                apiRecipeModel.preparationTime,
                apiRecipeModel.cookingTime,
                apiRecipeModel.servings,
                ingredientsList,
                instructionList,
                tagsList,
                mutableListOf(),
                apiRecipeModel.siteUrl
        )
    }

    // Function to extract the recipe_id from the given URL
    fun getIdFromUrl(url : String) : String? {
        val pattern = Regex("(?:https://)?(?:www\\.)?chefkoch\\.de/(?:amp/)?rezepte/(\\d+)/[\\w-]+\\.html.*")
        val match = pattern.matchEntire(url)

        return match?.groups?.get(1)?.value
    }
}
