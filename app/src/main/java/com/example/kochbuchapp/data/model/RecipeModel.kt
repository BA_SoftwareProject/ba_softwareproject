package com.example.kochbuchapp.data.model

import androidx.room.*
import com.example.kochbuchapp.data.db.Converters

@SuppressWarnings(RoomWarnings.PRIMARY_KEY_FROM_EMBEDDED_IS_DROPPED)
@TypeConverters(Converters::class)
@Entity(tableName = "recipe_table")
data class RecipeModel (
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "recipe_id")
    var id : Long = 0L,
    var title : String? = null,
    var subtitle : String? = null,
    @Embedded(prefix = "image_")
    var image : RecipeImageModel? = null,
    @ColumnInfo(name = "created_at")
    var createdAt: String? = null,
    @ColumnInfo(name = "preparation_time")
    var preparationTime : Int? = null,
    @ColumnInfo(name = "cooking_time")
    var cookingTime : Int? = null,
    var portions : Int? = null,
    @Ignore
    var ingredients : List<IngredientModel> = emptyList(),
    var instructions : List<String> = emptyList(),
    @Ignore
    var tags : List<TagModel> = emptyList(),
    var comments : List<String> = emptyList(),
    @ColumnInfo(name = "site_url")
    var siteUrl : String? = null
    )

data class RecipeModelFlat(
    val recipe_id : Long,
    val title : String?
)