package com.example.kochbuchapp.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.kochbuchapp.data.api.RestAPIClient
import com.example.kochbuchapp.data.dao.IngredientDao
import com.example.kochbuchapp.data.dao.RecipeDao
import com.example.kochbuchapp.data.dao.TagDao
import com.example.kochbuchapp.data.db.RecipeDatabase
import com.example.kochbuchapp.data.model.*

class RecipeRepository (context: Context) {
    private val recipeDao     : RecipeDao
    private val ingredientDao : IngredientDao
    private val tagDao        : TagDao
    private val apiClient = RestAPIClient()

    init {
        val db = RecipeDatabase.invoke(context)
        recipeDao     = db.recipeDao()
        ingredientDao = db.ingredientDAO()
        tagDao        = db.tagDao()
    }

    /* SAVE */

    fun saveRecipe(
        recipe : RecipeModel,
        updateTags : Boolean = true,
        updateIngredients : Boolean = true
    ) {
        // Is Recipe already available?
        if (isRecipeInTable(recipe)) {
        // Yes: update recipe
            recipeDao.update(recipe)

            if (updateIngredients) {
                recipe.ingredients.forEach { saveIngredient(it, recipe.id) }
            }

            if (updateTags) {
                recipeDao.getTagsInRecipe(recipe.id).forEach { deleteTag(it.tagId, recipe.id) }
                recipe.tags.forEach { saveTag(it, recipe.id) }
            }

        } else {
        // No: insert recipe
            val rid : Long = recipeDao.insert(recipe)

            recipe.ingredients.forEach { saveIngredient(it, rid) }
            recipe.tags.forEach { saveTag(it, rid) }
        }
    }

    private fun saveIngredient(ingredient: IngredientModel, recipeId: Long) {
        if (isIngredientInTable(ingredient)) {
            ingredientDao.update(ingredient.apply {
                this.recipeId = recipeId
            })
        } else {
            ingredientDao.insert(ingredient.apply{
                // Enable auto increment by setting primary key to 0
                ingredientId = 0L
                this.recipeId = recipeId
            })
        }
    }

    fun saveTag(tag: TagModel, recipeId: Long? = null) {
        // Insert or update tag in tag_table
        val tid : Long

        if (isTagInTable(tag)) {
            tagDao.update(tag)
            tid = tagDao.getTagByName(tag.name)!!.tagId
        } else {
            tid = tagDao.insert(tag.apply {
                // Enable auto increment by setting primary key to 0
                tagId = 0L
            })
        }

        // Insert tag/recipe-relation in crossRef_table
        if (recipeId != null) {
            recipeDao.insert(
                RecipeTagCrossRef(
                    recipe_id = recipeId,
                    tagId = tid
                )
            )
        }
    }

    fun updateTagPositions(tags : MutableList<TagModel>) {
        tagDao.updateTagPositions(tags)
    }

    /* DELETE */

    // function overload to delete tag by model
    private fun deleteTag(tag: TagModel, recipeId: Long) {
        // Delete tag in tag_table
        val tid : Long

        if (isTagInTable(tag)) {
            tid = tagDao.getTagByName(tag.name)!!.tagId

            // Delete tag/recipe-relation in crossRef_table
            recipeDao.delete(
                RecipeTagCrossRef(
                    recipe_id = recipeId,
                    tagId = tid
                )
            )
            if (recipeDao.getTagCount(tid) < 1) {
                tagDao.delete(tag)
            }
        }
    }

    // function overload to delete tag by name
    fun deleteTag(tagName: String, recipeId: Long) {
        // Delete tag in tag_table
        val tag : TagModel

        if (isTagInTable(tagName)) {
            tag = tagDao.getTagByName(tagName)!!

            // Delete tag/recipe-relation in crossRef_table
            recipeDao.delete(
                RecipeTagCrossRef(
                    recipe_id = recipeId,
                    tagId = tag.tagId
                )
            )
            if (recipeDao.getTagCount(tag.tagId) < 1) {
                tagDao.delete(tag)
            }
        }
    }

    // function overload to delete tag by name
    private fun deleteTag(tagId: Long, recipeId: Long) {
        // Delete tag in tag_table
        val tag : TagModel

        if (isTagInTable(tagId)) {
            tag = tagDao.getTagById(tagId)!!

            // Delete tag/recipe-relation in crossRef_table
            recipeDao.delete(
                RecipeTagCrossRef(
                    recipe_id = recipeId,
                    tagId = tagId
                )
            )
            if (recipeDao.getTagCount(tagId) < 1) {
                tagDao.delete(tag)
            }
        }
    }

    fun deleteRecipe(recipe : RecipeModel) {
        recipeDao.delete(recipe)

        recipe.ingredients.forEach { ingredient ->
            ingredientDao.delete(ingredient)
        }

        recipe.tags.forEach { tag ->
            deleteTag(tag, recipe.id)
        }
    }

    /* QUERY */

    fun getTagByName(tagName : String) : TagModel? {
        return tagDao.getTagByName(tagName)
    }

    fun getTags() : LiveData<List<TagModel>> {
        return tagDao.getTags()
    }

    /* UTIL */

    private fun isTagInTable(tag : TagModel) : Boolean {
        val testedTag = tagDao.getTagByName(tag.name)
        return testedTag != null
    }

    private fun isTagInTable(tagId : Long) : Boolean {
        val testedTag = tagDao.getTagById(tagId)
        return testedTag != null
    }

    private fun isTagInTable(tagName : String) : Boolean {
        val testedTag = tagDao.getTagByName(tagName)
        return testedTag != null
    }

    private fun isRecipeInTable(recipe : RecipeModel) : Boolean {
        val testedRecipe = recipeDao.getFlatRecipeById(recipe.id)
        return testedRecipe != null
    }

    private fun isIngredientInTable(ingredient : IngredientModel) : Boolean {
        val testedIngredient = ingredientDao.getIngredientById(ingredient.ingredientId)
        return testedIngredient != null
    }

    fun getRecipesLd(): LiveData<List<RecipeWithIngredientsAndTags>> {
        return recipeDao.getCompleteRecipesLd()
    }

    /* API */

    fun getIdFromUrl(url : String) : String? {
        return apiClient.getIdFromUrl(url)
    }

    suspend fun findRecipe(id : String, context: Context?) : RecipeModel? {
        return apiClient.findRecipe(id, context)
    }
}

