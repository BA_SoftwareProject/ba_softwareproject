package com.example.kochbuchapp.data.model

data class RecipeImageModel (
    val imageId : Long,
    val hasImage : Boolean,
    val isLocal : Boolean,
    val reference : String
)

