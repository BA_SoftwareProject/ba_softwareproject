package com.example.kochbuchapp.data.model

import androidx.room.*

data class RecipeWithTags (
    @Embedded
    val recipe : RecipeModel,
    @Relation(
        parentColumn = "recipe_id",
        entityColumn = "tag_id",
        associateBy = Junction(value = RecipeTagCrossRef::class)
    )
    val tags: List<TagModel>
)

//data class TagWithRecipes (
//    @Embedded
//    val tag : TagModel,
//    @Relation(
//        parentColumn = "tag_id",
//        entityColumn = "recipe_id",
//        associateBy = Junction(value = RecipeTagCrossRef::class)
//    )
//    val recipes: List<RecipeModel>
//)

@Entity(
    tableName = "recipe_tag_relation_table",
    primaryKeys = ["recipe_id", "tag_id"],
    indices = [Index(value = ["tag_id"])]
)
data class RecipeTagCrossRef (
    @ColumnInfo(name = "recipe_id")
    val recipe_id : Long,
    @ColumnInfo(name = "tag_id")
    val tagId : Long
)