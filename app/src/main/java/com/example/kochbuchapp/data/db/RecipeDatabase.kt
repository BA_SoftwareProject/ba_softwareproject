package com.example.kochbuchapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.kochbuchapp.data.dao.IngredientDao
import com.example.kochbuchapp.data.dao.RecipeDao
import com.example.kochbuchapp.data.dao.TagDao
import com.example.kochbuchapp.data.model.IngredientModel
import com.example.kochbuchapp.data.model.RecipeModel
import com.example.kochbuchapp.data.model.RecipeTagCrossRef
import com.example.kochbuchapp.data.model.TagModel
import com.example.kochbuchapp.helper.TestData
import com.example.kochbuchapp.helper.ioThread

@Database(
    entities = [
        RecipeModel::class,
        IngredientModel::class,
        TagModel::class,
        RecipeTagCrossRef::class
    ],
    version = 1
    )
abstract class RecipeDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao
    abstract fun ingredientDAO(): IngredientDao
    abstract fun tagDao(): TagDao

    companion object {
        @Volatile private var instance: RecipeDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context : Context) : RecipeDatabase = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                RecipeDatabase::class.java,
                "recipe.db"
            )
                .fallbackToDestructiveMigration() // TODO: remove fallback on release!
                .addCallback(object : Callback() {
                    override fun onOpen(db: SupportSQLiteDatabase) {
                        super.onOpen(db)

                        ioThread {
                            if (invoke(context).recipeDao().countRecipes() < 1)
                                TestData(context).fillDataBase()
                        }
                    }
                })
                .build()
    }
}