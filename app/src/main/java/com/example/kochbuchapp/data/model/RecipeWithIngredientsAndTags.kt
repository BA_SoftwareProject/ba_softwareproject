package com.example.kochbuchapp.data.model

import androidx.room.*

data class RecipeWithIngredientsAndTags (
    @Embedded
    val recipe : RecipeModel,

    @Relation(
        parentColumn = "recipe_id",
        entityColumn = "recipe_id",
    )
    val ingredients: List<IngredientModel>,

    @Relation(
        parentColumn = "recipe_id",
        entityColumn = "tag_id",
        associateBy = Junction(value = RecipeTagCrossRef::class)
    )
    val tags: List<TagModel>
) {
    fun toRecipeModel() : RecipeModel {
        return RecipeModel(
            id = this.recipe.id,
            title = this.recipe.title,
            subtitle = this.recipe.subtitle,
            image = this.recipe.image,
            createdAt = this.recipe.createdAt,
            preparationTime = this.recipe.preparationTime,
            cookingTime = this.recipe.cookingTime,
            portions = this.recipe.portions,
            ingredients = this.ingredients,
            instructions = this.recipe.instructions,
            tags = this.tags,
            comments = this.recipe.comments,
            siteUrl = this.recipe.siteUrl
        )
    }
}
