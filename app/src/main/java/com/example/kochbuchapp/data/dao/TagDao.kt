package com.example.kochbuchapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query import androidx.room.Transaction
import androidx.room.Update
import com.example.kochbuchapp.data.model.TagModel

@Dao
abstract class TagDao : BaseDao<TagModel> {
    // Get tag by name
    @Query("SELECT * FROM tag_table WHERE name = :name")
    abstract fun getTagByName(name: String) : TagModel?

    // Get tag by id
    @Query("SELECT * FROM tag_table WHERE tag_id = :id")
    abstract fun getTagById(id: Long) : TagModel?

    @Query("SELECT * FROM tag_table")
    abstract fun getTags() : LiveData<List<TagModel>>

    @Update
    abstract fun updateTagPositions(tags : MutableList<TagModel>)
}