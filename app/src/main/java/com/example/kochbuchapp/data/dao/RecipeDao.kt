package com.example.kochbuchapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.kochbuchapp.data.model.*

@Dao
abstract class RecipeDao : BaseDao<RecipeModel> {
    // Get all Recipes with corresponding Ingredient list
    @Transaction
    @Query("SELECT * FROM recipe_table")
    abstract fun getRecipes(): List<RecipeWithIngredients>

    // Get all Recipes with corresponding tag list
    @Transaction
    @Query("SELECT * FROM recipe_table")
    abstract fun getRecipesWithTags(): List<RecipeWithTags>

    @Query("SELECT COUNT(*) FROM recipe_table")
    abstract fun countRecipes() : Int

    // Get all Recipes with corresponding Ingredient list and Tag list
    @Transaction
    @Query("SELECT * FROM recipe_table")
    abstract fun getCompleteRecipes(): List<RecipeWithIngredientsAndTags>

    // Get all Recipes as LiveData with corresponding Ingredient list and Tag list
    @Transaction
    @Query("SELECT * FROM recipe_table")
    abstract fun getCompleteRecipesLd(): LiveData<List<RecipeWithIngredientsAndTags>>

    // Get one single complete recipe by Id
    @Transaction
    @Query("SELECT * FROM recipe_table WHERE recipe_id = :id")
    abstract fun getRecipeById(id: Long) : RecipeWithIngredientsAndTags

    // Get one single incomplete recipe by Id (just for checking if recipe_id already exists)
    @Query("SELECT recipe_id, title FROM recipe_table WHERE recipe_id = :id")
    abstract fun getFlatRecipeById(id: Long) : RecipeModelFlat?

    // Get a list of complete Recipes by search parameter
    @Transaction
    @Query("SELECT * FROM recipe_table WHERE LOWER(title) LIKE LOWER(:search)")
    abstract fun getRecipesByTitle(search: String) : List<RecipeWithIngredientsAndTags>

    /* CrossRef-Queries */

    // Insert a Tag and Recipe CrossRef Entity
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(entity: RecipeTagCrossRef)

    // Delete a Tag and Recipe CrossRef Entity
    @Delete
    abstract fun delete(entity: RecipeTagCrossRef)

    // Count tag_id-entries in recipe_tag_relation_table
    @Query("SELECT COUNT(tag_id) FROM recipe_tag_relation_table WHERE tag_id = :id")
    abstract fun getTagCount(id : Long): Int

    // Count tag_id-entries in recipe in recipe_tag_relation_table
    @Query("SELECT COUNT(tag_id) FROM recipe_tag_relation_table WHERE recipe_id = :recipeId")
    abstract fun getTagCountInRecipe(recipeId : Long): Int

    // Get tag-entries in recipe in recipe_tag_relation_table
    @Query("SELECT * FROM recipe_tag_relation_table WHERE recipe_id = :recipeId")
    abstract fun getTagsInRecipe(recipeId : Long): List<RecipeTagCrossRef>
}