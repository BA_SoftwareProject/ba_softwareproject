package com.example.kochbuchapp.data.model

import androidx.room.*

data class RecipeWithIngredients (
    @Embedded
    val recipe : RecipeModel,
    @Relation(
        parentColumn = "recipe_id",
        entityColumn = "recipe_id",
    )
    val ingredients: List<IngredientModel>
)