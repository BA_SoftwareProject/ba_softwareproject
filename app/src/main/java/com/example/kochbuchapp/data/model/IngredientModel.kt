package com.example.kochbuchapp.data.model

import androidx.room.*

@Entity(tableName = "ingredient_table")
data class IngredientModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ingredient_id")
    var ingredientId : Long,
    @ColumnInfo(name = "recipe_id")
    var recipeId : Long?,
    var name: String,
    var unit: String,
    var amount: Double,
    @ColumnInfo(name = "product_group")
    var productGroup: String?
) {
    constructor(ingredient : IngredientModel) : this(
        ingredientId = ingredient.ingredientId,
        recipeId = ingredient.recipeId,
        name = ingredient.name,
        unit = ingredient.unit,
        amount = ingredient.amount,
        productGroup = ingredient.productGroup
    )
}