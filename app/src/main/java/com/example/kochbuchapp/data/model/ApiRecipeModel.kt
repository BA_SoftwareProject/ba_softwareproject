package com.example.kochbuchapp.data.model

data class ApiRecipeModel (
    val id : Long,
    val title : String,
    val subtitle : String,
    val hasImage : Boolean,
    val previewImageId : Int,
    val preparationTime : Int,
    val createdAt : String,
    val servings : Int,
    val instructions : String,
    val tags : List<String>,
    val fullTags : List<ApiTag>,
    val cookingTime : Int,
    val restingTime : Int,
    val totalTime : Int,
    val ingredientGroups : List<IngredientGroups>,
    val siteUrl : String
)

data class IngredientGroups (
    val ingredients : List<IngredientModel>
)

data class ApiTag (
    val id: Long,
    val name : String
)