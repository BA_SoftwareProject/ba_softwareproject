package com.example.kochbuchapp.data.db

import androidx.room.TypeConverter

// this class controls the conversions of string lists to strings and vice versa
class Converters {
    @TypeConverter
    fun fromString(str: String?): MutableList<String> {
        return str?.split("__", limit = 0) as MutableList<String>
    }

    @TypeConverter
    fun fromMutableList(list: MutableList<String?>?): String {
        var output = ""
        list?.forEachIndexed { index, str ->
            output += if (index < list.size-1) "${str}__"
            else "$str"
        }
        return output
    }
}