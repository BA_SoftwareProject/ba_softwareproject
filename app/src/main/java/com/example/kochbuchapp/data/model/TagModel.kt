package com.example.kochbuchapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tag_table")
class TagModel (
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "tag_id")
        var tagId : Long,
        val name : String,
        @ColumnInfo(name = "list_position")
        var listPosition : Int?,
        var isChecked : Boolean?
    )