package com.example.kochbuchapp.data.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update

// Reference:
// https://gist.github.com/florina-muntenescu/1c78858f286d196d545c038a71a3e864
// https://medium.com/androiddevelopers/7-pro-tips-for-room-fbadea4bfbd1

interface BaseDao<T> {
    /**
     * Insert an object in the database.
     * @param obj the object to be inserted.
     */
    @Insert
    fun insert(obj: T) : Long

    /**
     * Insert an array of objects in the database.
     * @param obj the objects to be inserted.
     */
    @Insert
    fun insert(vararg obj: T) : List<Long>

    /**
     * Update an object from the database.
     * @param obj the object to be updated
     */
    @Update
    fun update(obj: T)

    /**
     * Update an array of objects from the database.
     * @param obj the objects to be updated
     */
    @Update
    fun update(vararg obj: T)

    /**
     * Delete an object from the database
     * @param obj the object to be deleted
     */
    @Delete
    fun delete(obj: T)
}