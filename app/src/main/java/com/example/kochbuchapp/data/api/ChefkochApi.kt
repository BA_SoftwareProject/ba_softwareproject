package com.example.kochbuchapp.data.api

import com.example.kochbuchapp.data.model.ApiRecipeModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ChefKochApi {

    @GET("/v2/recipes/{recipe_id}")
    suspend fun getRecipe(@Path("recipe_id") recipe_id : String): Response<ApiRecipeModel>
}
