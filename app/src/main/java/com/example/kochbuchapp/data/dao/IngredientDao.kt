package com.example.kochbuchapp.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.kochbuchapp.data.model.IngredientModel

@Dao
abstract class IngredientDao : BaseDao<IngredientModel> {
    // Get one Ingredient by Id
    @Query("SELECT * FROM ingredient_table WHERE ingredient_id = :id")
    abstract fun getIngredientById(id: Long) : IngredientModel?
}